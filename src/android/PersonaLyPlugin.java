package com.phonegap.plugins;

import ly.persona.sdk.PersonaSdk;
import ly.persona.sdk.OffersActivity;
import ly.persona.sdk.util.PersonaConstants;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class PersonaLyPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callback) throws JSONException {
    	Log.v("PersonaLyPlugin", "PersonaLyPlugin execute method called (action = " + action + ")");
        
        try {
            if (action.equals("init")) {
                Log.d("PersonaLyPlugin", "init(" + args.getString(0) + ", " + args.getString(0) + ")");
            	PersonaSdk.init(cordova.getActivity().getApplicationContext(), args.getString(0), args.getString(1));
            } else if (action.equals("initNoId")) {
                Log.d("PersonaLyPlugin", "initNoId(" + args.getString(0) + ")");
            	PersonaSdk.initNoId(cordova.getActivity().getApplicationContext(), args.getString(0));
            } else if (action.equals("setUserId")) {
            	PersonaSdk.getInstance().setUserId(args.getString(0));
            } else if(action.equals("showOffers")) {
                PersonaSdk.showOffers();
            } else if(action.equals("setGender")) {
                if (args.getString(0) == "m")
                    PersonaSdk.getInstance().setGender(PersonaConstants.GENDER_MAN);
                else
                    PersonaSdk.getInstance().setGender(PersonaConstants.GENDER_FEMALE);
        	} else {
                Log.d("PersonaLyPlugin", "invalid PersonaLy method: " + action);
                callback.error("invalid PersonaLy method: " + action);
        		return false;
        	}
            callback.success();
            return true; 
        } catch (JSONException e) {
            Log.d("PersonaLyPlugin exception: ", e.getMessage());
            callback.error("PersonaLyPlugin json exception: " + e.getMessage());
            return false;
        }
    }
}
