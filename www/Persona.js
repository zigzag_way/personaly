var PersonaLy = {
    init: function(successCallback, failureCallback, appHash, userId) {
        return cordova.exec(successCallback, failureCallback, 'PersonaLyPlugin', 'init', [appHash, userId]);
    },
    initNoId: function(successCallback, failureCallback, appHash) {
        return cordova.exec(successCallback, failureCallback, 'PersonaLyPlugin', 'initNoId', [appHash]);
    },
    setUserId: function(successCallback, failureCallback) {
        return cordova.exec(successCallback, failureCallback, 'PersonaLyPlugin', 'setUserId', []);
    },
    showOffers: function(successCallback, failureCallback) {
        return cordova.exec(successCallback, failureCallback, 'PersonaLyPlugin', 'showOffers', []);
    },
    setGender: function(successCallback, failureCallback, gender) {
        return cordova.exec(successCallback, failureCallback, 'PersonaLyPlugin', 'setGender', [gender]);
    },
}
module.exports = PersonaLy;
